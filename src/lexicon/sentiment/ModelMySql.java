/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lexicon.sentiment;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mfahry
 */
public class ModelMySql {
    String username;
    String password;
    Connection con;
    PreparedStatement state;
    
    public ModelMySql(String username, String password) {
        this.username = username;
        this.password = password;
        
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/lexicon",this.username,this.password); 
        } catch (ClassNotFoundException | SQLException e) {
            System.out.print("error message :"+e);
        }
    }

    public int manipulation(String query, String[] param){
        try {
            state = con.prepareStatement(query);
            
            //set parameter masukan
            for(int i = 0; i < param.length; i++){
                state.setString(i + 1, param[i]);
            }
            
            return state.executeUpdate();
        } 
        
        catch (SQLException ex) {
            Logger.getLogger(ModelMySql.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return 2; //jika tidak ada perubahan yang terjadi
    }  
    
    public ResultSet view(String query, String[] param){
        try {
            state = con.prepareStatement(query);
            
            //set parameter masukan
            for(int i = 0; i< param.length; i++){
                state.setString(i + 1, param[i]);
            }
            
            return state.executeQuery();
        } 
        
        catch (SQLException ex) {
            Logger.getLogger(ModelMySql.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}