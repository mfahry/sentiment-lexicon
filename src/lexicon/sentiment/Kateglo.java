/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lexicon.sentiment;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.*;
/**
 *
 * @author mfahry
 */
public class Kateglo {
    String stringUrl; 
    String phrase;
    String format;
    ArrayList<String> lexicon;

    public Kateglo(String stringUrl, String phrase, String format) {
        this.stringUrl = stringUrl;
        this.phrase = phrase;
        this.format = format;
    }
    
    public ArrayList<String> getLexicon(){
        try{
            //buat sebuah kumpulan lexicon kosong 
            lexicon = new ArrayList<>();
            
            //set url dari format dan phrase        
            stringUrl += "format="+format;
            stringUrl += "&phrase="+phrase;
            
            //baca response data dari url
            URL url = new URL(stringUrl);
            Scanner scanResponse = new Scanner(url.openStream());
            String response = new String();
            while(scanResponse.hasNext()){
                response += scanResponse.nextLine();
            }
            scanResponse.close();
            
            //jika relation dari kateglo ada
            if(response.contains("all_relation")) {
                //bentuk sebuah JSONObject dari response
                JSONObject json = new JSONObject(response);

                //ambil JSONArray "all_relation dari json"
                JSONArray relation = json.getJSONObject("kateglo").getJSONArray("all_relation");

                //ambil String dari "related_phrase" jika merupakan adj dan sinonim
                for(int i = 0 ; i < relation.length(); i++){
                    if( 
                        relation.getJSONObject(i).get("rel_type").toString().equals("s") &&  
                        relation.getJSONObject(i).get("lex_class").toString().equals("adj")
                    ) {  
                        lexicon.add(relation.getJSONObject(i).getString("related_phrase"));
                    }
                }
            }
        }   
        
        catch(UnsupportedEncodingException | MalformedURLException e){
            System.out.print("error message : "+e);
        } 
        
        catch (IOException ex) {
            Logger.getLogger(Kateglo.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return lexicon;
    }
    
    
    
   /*URL url = new URL(s);
 
    // read from the URL
    Scanner scan = new Scanner(url.openStream());
    String str = new String();
    while (scan.hasNext())
        str += scan.nextLine();
    scan.close();
 
    // build a JSON object
    JSONObject obj = new JSONObject(str);
    if (! obj.getString("status").equals("OK"))
        return;
 
    // get the first result
    JSONObject res = obj.getJSONArray("results").getJSONObject(0);
    System.out.println(res.getString("formatted_address"));
    JSONObject loc =
        res.getJSONObject("geometry").getJSONObject("location");
    System.out.println("lat: " + loc.getDouble("lat") +
                        ", lng: " + loc.getDouble("lng"));*/
}
