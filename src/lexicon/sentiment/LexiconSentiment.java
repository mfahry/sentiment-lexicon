/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lexicon.sentiment;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mfahry
 */
public class LexiconSentiment {

    private MyWindows myWindows;
    private String phrase;
    private ModelMySql db;
    private int totalPositive;
    private int totalNegative;
    private Thread threadProcess;
    private Thread threadChange;
    public boolean isLife;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        MyWindows windows = new MyWindows();
        windows.main();
    }

    public LexiconSentiment(MyWindows myWindows) {
        this.myWindows = myWindows;
        isLife = true;
        
        db = new ModelMySql("root", "");
        // Delet First dimatiin karena jika udah ada data didatabase biar bisa distop dulu untuk clean
        
        /*String[] paramEmptyInit = new String[0];
        db.manipulation("DELETE FROM sentiment", paramEmptyInit);*/
        
    }

    public void startProcess(String superSeed, final String type) {
        
        //Insert First
        String[] paramInit = {superSeed, "0", type};
        db.manipulation("INSERT IGNORE INTO sentiment (word, status_used, type) VALUES (?,?,?)", paramInit);
        
        final String url = "http://kateglo.com/api.php?";
        final String format = "json";
        phrase = getSeed(db, type);

        threadProcess = new Thread(new Runnable() {
            @Override
            public void run() {
                //lakukan pencarian 
                while (phrase != null && isLife) {
                    try {
                        Scanner input = new Scanner(System.in);
                        System.out.println("Seed :" + phrase + " Jenis " + type);
                        doChange(type);
                        ArrayList<String> lexicon = new ArrayList<>();

                        Kateglo kateglo = new Kateglo(url, phrase, format);
                        lexicon = kateglo.getLexicon();

                        for (int i = 0; i < lexicon.size(); i++) {
                            String[] param = {lexicon.get(i), type};
                            db.manipulation("INSERT IGNORE INTO temp_sentiment (word, type) VALUES (?,?)", param);
                        }

                        String[] param = {"1", phrase, type};
                        db.manipulation("UPDATE sentiment SET status_used = ? WHERE word = ? and type = ?", param);

                        String[] paramEmpty = new String[0];
                        db.manipulation(
                                "INSERT INTO sentiment "
                                + "SELECT a.word, '0', a.type FROM temp_sentiment a LEFT JOIN sentiment b ON(a.word = b.word) WHERE b.word is null", paramEmpty);

                        db.manipulation("DELETE FROM temp_sentiment", paramEmpty);
                        threadProcess.sleep(10000);

                        phrase = getSeed(db, type);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(LexiconSentiment.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });
        threadProcess.start();
        
    }

    public String getSeed(ModelMySql db, String type) {

        try {
            //dapatkan seed yang belum dipakai
            String[] param = {"0", type};
            ResultSet rs = db.view("SELECT * FROM sentiment WHERE status_used = ? and type = ? ORDER BY RAND() LIMIT 1", param);
            if (rs.next()) {
                return rs.getString("word");
            }
        } catch (SQLException ex) {
            Logger.getLogger(LexiconSentiment.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    public void doChange(final String type) {
        threadChange = new Thread(new Runnable() {
            @Override
            public void run() {
                if (type.equals("positive")) {
                    totalPositive++;
                    myWindows.setPositivResult(phrase, totalPositive);
                } else {
                    totalNegative++;
                    myWindows.setNegatifResult(phrase, totalNegative);
                }
            }
        });

        threadChange.start();
    }

    public void stopAll() {
//        phrase = null;
        isLife = false;
//        threadChange.stop();
//        threadProcess.stop();
    }

}
