/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lexicon.sentiment;

import java.io.File;

/**
 *
 * @author Akip Maulana
 */
public class MyCustomFilter extends javax.swing.filechooser.FileFilter {

    private int fileType;

    public MyCustomFilter(int fileType) {
        this.fileType = fileType;
    }

    @Override
    public boolean accept(File file) {
        if (fileType == 1) {
            // Allow only directories, or files with ".c" extension
            return file.isDirectory() || file.getAbsolutePath().endsWith(".txt");
        } else {
            // Allow only directories, or files with ".c" extension
            return file.isDirectory() || file.getAbsolutePath().endsWith(".txt");
        }

    }

    @Override
    public String getDescription() {
            // This description will be displayed in the dialog,
        // hard-coded = ugly, should be done via I18N
        if (fileType == 1) {
            return "Text documents (*.txt)";
        } else {
            return "Text documents (*.txt)";
        }
    }
}
